﻿var express = require('express');
var vhost = require('vhost');
var path = require('path');
var http = require('http');
var https = require('https');
var favicon = require('serve-favicon');
var ejs = require('ejs');
var fs = require('fs');
var app = express();
app.set('view engine', 'ejs');
app.set("views", __dirname +"/public");


// //  
// https.createServer({
    // key: fs.readFileSync('/home/ec2-user/wewalkthru.com.key'),
    // cert: fs.readFileSync('/home/ec2-user/wewalkthru.com.crt')
// }, app).listen(444);

app.set('port', process.env.PORT || 3004);

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));

app.use('/asset',express.static(__dirname + '/public/asset'));
app.use('/css',express.static(__dirname + '/public/css'));
app.use('/images',express.static(__dirname + '/public/images'));
app.use('/js', express.static(__dirname + '/public/js'));
app.use('/fonts', express.static(__dirname + '/public/fonts'));
app.use('/', express.static(__dirname + '/public/'));
//Store all JS and CSS in Scripts folder.
app.use(express.static(__dirname + '../public'));

app.use(function(req, res, next) {
   res.locals.query = req.query;
   res.locals.url   = req.originalUrl;

   next();
});
app.get('/', function (req, res) {
    res.render(__dirname + '/public/login');
});

app.get('/login', function (req, res) {
    res.render(__dirname + '/public/login');
});
/*
app.get('/index.html', function (req, res) {
    res.sendFile(path.join(__dirname + '/public/index.html'));
});
*/
app.get('/dashboard', function (req, res) {
    res.render(__dirname + '/public/dashboard');
});
app.get('/adduser', function (req, res) {
    res.render(__dirname + '/public/adduser');
});
app.get('/users', function (req, res) {
    res.render(__dirname + '/public/users');
});

app.get('/listwalkthru', function (req, res) {
    res.render(__dirname + '/public/listwalkthru');
});
app.get('/companylist', function (req, res) {
    res.render(__dirname + '/public/companylist');
});
app.get('/addcompany', function (req, res) {
    res.render(__dirname + '/public/addcompany');
});
app.get('/analytics', function (req, res) {
    res.render(__dirname + '/public/analytics');
});
app.get('/editprofile', function (req, res) {
    res.render(__dirname + '/public/editprofile');
});
app.get('/edituser', function (req, res) {
    res.render(__dirname + '/public/edituser');
});

app.get('/editcompany', function (req, res) {
    res.render(__dirname + '/public/editcompany');
});
app.get('/transferwalkthru', function (req, res) {
    res.render(__dirname + '/public/transfer');
});
app.get('/migratewalkthru', function (req, res) {
    res.render(__dirname + '/public/migratewalkthru');
});
app.get('/billing', function (req, res) {
    res.render(__dirname + '/public/billing');
});
app.get('/embedscript', function (req, res) {
    res.render(__dirname + '/public/embedscript');
});


app.get('/forgot', function (req, res) {
    res.render(__dirname +'/public/forgot');
});

app.get('/customizepopover', function (req, res) {
    res.render(__dirname +'/public/popover');
});

app.get('/emailverified', function (req, res) {
    res.render(__dirname +'/public/emailverified');
});
app.get('/invalidlink', function (req, res) {
    res.render(__dirname +'/public/invalidlink');
});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
module.exports = app;
