
/*--------------CONTROLS----------------------*/

var globalSelector;
var Css = {
    popover: { hasProperties: false, backgroundcolor: null, borderRadius: null,paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    nextButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    endButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    backButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    closeButton: { hasProperties: false, backgroundcolor: null, color: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    stepsTitle: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    heading: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    content: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    progressbar: { hasProperties: false, backgroundcolor: null, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    logoText:{hasProperties:false,color:null}
};


var stepbackarr = [];

function resetCss(){
    Css = {
    popover: { hasProperties: false, backgroundcolor: null, borderRadius: null,paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    nextButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    endButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    backButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    closeButton: { hasProperties: false, backgroundcolor: null, color: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    stepsTitle: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    heading: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    content: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    progressbar: { hasProperties: false, backgroundcolor: null, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    logoText:{hasProperties:false,color:null}
};
$("#popover-custom-wt").html(" ");
    $("#popover-custom-wt").remove();
return true;
}

$("#undo-wt").on('click',function(){
    if(stepbackarr.length>=1){
    UndoAction();
    showComponenCss();
    }
})

function UndoAction(){
    var index = stepbackarr.length-2;
    Css = JSON.parse(stepbackarr[index]);
    ParseTempCss();
    stepbackarr.pop();
}
//parsing Css Object to temp css
function ParseTempCss(){
    //popover CSS
    if(Css.popover.hasProperties){
        if(Css.popover.backgroundcolor!=null){
            $(".popover-wt").css("background-color",Css.popover.backgroundcolor);
            changearrowColor(Css.popover.backgroundcolor);
        }
        if(Css.popover.borderRadius!=null){
            $(".popover-wt").css("border-radius",Css.popover.borderRadius);
            
        }
        if(Css.popover.paddingT!=null){
             $(".popover-wt").css("padding-top",Css.popover.paddingT);
        }
        if(Css.popover.paddingL!=null){
             $(".popover-wt").css("padding-left",Css.popover.paddingL);
        }
        if(Css.popover.paddingB!=null){
             $(".popover-wt").css("padding-bottom",Css.popover.paddingB);
        }
        if(Css.popover.paddingR!=null){
             $(".popover-wt").css("padding-right",Css.popover.paddingR);
        }
        if(Css.popover.marginT!=null){
             $(".popover-wt").css("margin-top",Css.popover.marginT);
        }
        if(Css.popover.marginL!=null){
             $(".popover-wt").css("margin-left",Css.popover.marginL);
        }
        if(Css.popover.marginB!=null){
             $(".popover-wt").css("margin-bottom",Css.popover.marginB);
        }
        if(Css.popover.marginR!=null){
             $(".popover-wt").css("margin-right",Css.popover.marginR);
        }
    }
    //Next Button CSS
    if(Css.nextButton.hasProperties){
        if(Css.nextButton.backgroundcolor!=null){
            $(".next").css("background-color",Css.nextButton.backgroundcolor);
            $(".next").css("border-color",Css.nextButton.backgroundcolor);
        } 
        if(Css.nextButton.color!=null){
             $(".next").css("color",Css.nextButton.color);
        }
        if(Css.nextButton.hover!=null){
             $('head').append("<style id='custom-style'>:root .popover-wt .next:hover{background-color:" + Css.nextButton.hover + "!important;}</style>");
        }
        if(Css.nextButton.borderRadius!=null){
             $(".next").css("border-radius",Css.nextButton.borderRadius);
        }
        if(Css.nextButton.font!=null){
             $(".next").css("font-family",Css.nextButton.font);
        }
        if(Css.nextButton.fontSize!=null){
             $(".next").css("font-size",Css.nextButton.fontSize);
        }
        if(Css.nextButton.fontWeight!=null){
             $(".next").css("font-weight",Css.nextButton.fontWeight);
        }
        if(Css.nextButton.paddingT!=null){
             $(".next").css("padding-top",Css.nextButton.paddingT);
        }
        if(Css.nextButton.paddingL!=null){
             $(".next").css("padding-left",Css.nextButton.paddingL);
        }
        if(Css.nextButton.paddingB!=null){
             $(".next").css("padding-bottom",Css.nextButton.paddingB);
        }
        if(Css.nextButton.paddingR!=null){
             $(".next").css("padding-right",Css.nextButton.paddingR);
        }
        if(Css.nextButton.marginT!=null){
             $(".next").css("margin-top",Css.nextButton.marginT);
        }
        if(Css.nextButton.marginL!=null){
             $(".next").css("margin-left",Css.nextButton.marginL);
        }
        if(Css.nextButton.marginB!=null){
             $(".next").css("margin-bottom",Css.nextButton.marginB);
        }
        if(Css.nextButton.marginR!=null){
             $(".next").css("margin-right",Css.nextButton.marginR);
        }
    }
    //Back Button CSS
    if(Css.backButton.hasProperties){
        if(Css.backButton.backgroundcolor!=null){
            $(".back").css("background-color",Css.backButton.backgroundcolor);
            $(".back").css("border-color",Css.backButton.backgroundcolor);
        } 
        if(Css.backButton.color!=null){
             $(".back").css("color",Css.backButton.color);
        }
        if(Css.backButton.hover!=null){
             $('head').append("<style id='custom-style'>:root .popover-wt .back:hover{background-color:" + Css.backButton.hover + "!important;}</style>");
        }
        if(Css.backButton.borderRadius!=null){
             $(".back").css("border-radius",Css.backButton.borderRadius);
        }
        if(Css.backButton.font!=null){
             $(".back").css("font-family",Css.backButton.font);
        }
        if(Css.backButton.fontSize!=null){
             $(".back").css("font-size",Css.backButton.fontSize);
        }
        if(Css.backButton.fontWeight!=null){
             $(".back").css("font-weight",Css.backButton.fontWeight);
        }
        if(Css.backButton.paddingT!=null){
             $(".back").css("padding-top",Css.backButton.paddingT);
        }
        if(Css.backButton.paddingL!=null){
             $(".back").css("padding-left",Css.backButton.paddingL);
        }
        if(Css.backButton.paddingB!=null){
             $(".back").css("padding-bottom",Css.backButton.paddingB);
        }
        if(Css.backButton.paddingR!=null){
             $(".back").css("padding-right",Css.backButton.paddingR);
        }
        if(Css.backButton.marginT!=null){
             $(".back").css("margin-top",Css.backButton.marginT);
        }
        if(Css.backButton.marginL!=null){
             $(".back").css("margin-left",Css.backButton.marginL);
        }
        if(Css.backButton.marginB!=null){
             $(".back").css("margin-bottom",Css.backButton.marginB);
        }
        if(Css.backButton.marginR!=null){
             $(".back").css("margin-right",Css.backButton.marginR);
        }
    }
    //End Button CSS
    if(Css.endButton.hasProperties){
        if(Css.endButton.backgroundcolor!=null){
            $(".end").css("background-color",Css.endButton.backgroundcolor);
            $(".end").css("border-color",Css.endButton.backgroundcolor);
        } 
        if(Css.endButton.color!=null){
             $(".end").css("color",Css.endButton.color);
        }
        if(Css.endButton.hover!=null){
             $('head').append("<style id='custom-style'>:root .popover-wt .end:hover{background-color:" + Css.endButton.hover + "!important;}</style>");
        }
        if(Css.endButton.borderRadius!=null){
             $(".end").css("border-radius",Css.endButton.borderRadius);
        }
        if(Css.endButton.font!=null){
             $(".end").css("font-family",Css.endButton.font);
        }
        if(Css.endButton.fontSize!=null){
             $(".end").css("font-size",Css.endButton.fontSize);
        }
        if(Css.endButton.fontWeight!=null){
             $(".end").css("font-weight",Css.endButton.fontWeight);
        }
        if(Css.endButton.paddingT!=null){
             $(".end").css("padding-top",Css.endButton.paddingT);
        }
        if(Css.endButton.paddingL!=null){
             $(".end").css("padding-left",Css.endButton.paddingL);
        }
        if(Css.endButton.paddingB!=null){
             $(".end").css("padding-bottom",Css.endButton.paddingB);
        }
        if(Css.endButton.paddingR!=null){
             $(".end").css("padding-right",Css.endButton.paddingR);
        }
        if(Css.endButton.marginT!=null){
             $(".end").css("margin-top",Css.endButton.marginT);
        }
        if(Css.endButton.marginL!=null){
             $(".end").css("margin-left",Css.endButton.marginL);
        }
        if(Css.endButton.marginB!=null){
             $(".end").css("margin-bottom",Css.endButton.marginB);
        }
        if(Css.endButton.marginR!=null){
             $(".end").css("margin-right",Css.endButton.marginR);
        }
    }
    //Close Button CSS
    if(Css.closeButton.hasProperties){
        if(Css.closeButton.backgroundcolor!=null){
            $(".close-wt").css("background-color",Css.closeButton.backgroundcolor);
            $(".close-wt").css("border-color",Css.closeButton.backgroundcolor);
        } 
        if(Css.closeButton.color!=null){
             $(".close-wt").css("color",Css.closeButton.color);
        }
        if(Css.closeButton.hover!=null){
             $('head').append("<style id='custom-style'>:root .popover-wt .close-wt:hover{background-color:" + Css.closeButton.hover + "!important;}</style>");
        }
        if(Css.closeButton.borderRadius!=null){
             $(".close-wt").css("border-radius",Css.closeButton.borderRadius);
        }
        if(Css.closeButton.font!=null){
             $(".close-wt").css("font-family",Css.closeButton.font);
        }
        if(Css.closeButton.fontSize!=null){
             $(".close-wt").css("font-size",Css.closeButton.fontSize);
        }
        if(Css.closeButton.fontWeight!=null){
             $(".close-wt").css("font-weight",Css.closeButton.fontWeight);
        }
        if(Css.closeButton.paddingT!=null){
             $(".close-wt").css("padding-top",Css.closeButton.paddingT);
        }
        if(Css.closeButton.paddingL!=null){
             $(".close-wt").css("padding-left",Css.closeButton.paddingL);
        }
        if(Css.closeButton.paddingB!=null){
             $(".close-wt").css("padding-bottom",Css.closeButton.paddingB);
        }
        if(Css.closeButton.paddingR!=null){
             $(".close-wt").css("padding-right",Css.closeButton.paddingR);
        }
        if(Css.closeButton.marginT!=null){
             $(".close-wt").css("margin-top",Css.closeButton.marginT);
        }
        if(Css.closeButton.marginL!=null){
             $(".close-wt").css("margin-left",Css.closeButton.marginL);
        }
        if(Css.closeButton.marginB!=null){
             $(".close-wt").css("margin-bottom",Css.closeButton.marginB);
        }
        if(Css.closeButton.marginR!=null){
             $(".close-wt").css("margin-right",Css.closeButton.marginR);
        }
    }
     //StepInfo CSS e.g. Step 1 of 1
    if(Css.stepsTitle.hasProperties){
       
        if(Css.stepsTitle.color!=null){
             $(".popover-wt-title").css("color",Css.stepsTitle.color);
        }
        
        if(Css.stepsTitle.font!=null){
             $(".popover-wt-title").css("font-family",Css.stepsTitle.font);
        }
        if(Css.stepsTitle.fontSize!=null){
             $(".popover-wt-title").css("font-size",Css.stepsTitle.fontSize);
        }
        if(Css.stepsTitle.fontWeight!=null){
             $(".popover-wt-title").css("font-weight",Css.stepsTitle.fontWeight);
        }
        if(Css.stepsTitle.paddingT!=null){
             $(".popover-wt-title").css("padding-top",Css.stepsTitle.paddingT);
        }
        if(Css.stepsTitle.paddingL!=null){
             $(".popover-wt-title").css("padding-left",Css.stepsTitle.paddingL);
        }
        if(Css.stepsTitle.paddingB!=null){
             $(".popover-wt-title").css("padding-bottom",Css.stepsTitle.paddingB);
        }
        if(Css.stepsTitle.paddingR!=null){
             $(".popover-wt-title").css("padding-right",Css.stepsTitle.paddingR);
        }
        if(Css.stepsTitle.marginT!=null){
             $(".popover-wt-title").css("margin-top",Css.stepsTitle.marginT);
        }
        if(Css.stepsTitle.marginL!=null){
             $(".popover-wt-title").css("margin-left",Css.stepsTitle.marginL);
        }
        if(Css.stepsTitle.marginB!=null){
             $(".popover-wt-title").css("margin-bottom",Css.stepsTitle.marginB);
        }
        if(Css.stepsTitle.marginR!=null){
             $(".popover-wt-title").css("margin-right",Css.stepsTitle.marginR);
        }
    }
     //Heading CSS Cases not tested are aligned and non aligned
    if(Css.heading.hasProperties){
       
        if(Css.heading.color!=null){
             $(".popover-wt-heading").css("color",Css.heading.color);
        }
        
        if(Css.heading.font!=null){
             $(".popover-wt-heading").css("font-family",Css.heading.font);
        }
        if(Css.heading.fontSize!=null){
             $(".popover-wt-heading").css("font-size",Css.heading.fontSize);
        }
        if(Css.heading.fontWeight!=null){
             $(".popover-wt-heading").css("font-weight",Css.heading.fontWeight);
        }
        if(Css.heading.paddingT!=null){
             $(".popover-wt-heading").css("padding-top",Css.heading.paddingT);
        }
        if(Css.heading.paddingL!=null){
             $(".popover-wt-heading").css("padding-left",Css.heading.paddingL);
        }
        if(Css.heading.paddingB!=null){
             $(".popover-wt-heading").css("padding-bottom",Css.heading.paddingB);
        }
        if(Css.heading.paddingR!=null){
             $(".popover-wt-heading").css("padding-right",Css.heading.paddingR);
        }
        if(Css.heading.marginT!=null){
             $(".popover-wt-heading").css("margin-top",Css.heading.marginT);
        }
        if(Css.heading.marginL!=null){
             $(".popover-wt-heading").css("margin-left",Css.heading.marginL);
        }
        if(Css.heading.marginB!=null){
             $(".popover-wt-heading").css("margin-bottom",Css.heading.marginB);
        }
        if(Css.heading.marginR!=null){
             $(".popover-wt-heading").css("margin-right",Css.heading.marginR);
        }
    }
     //Heading CSS Cases not tested are aligned and non aligned
    if(Css.content.hasProperties){
       
        if(Css.content.color!=null){
             $(".popover-wt-content").css("color",Css.content.color);
        }
        
        if(Css.content.font!=null){
             $(".popover-wt-content").css("font-family",Css.content.font);
        }
        if(Css.content.fontSize!=null){
             $(".popover-wt-content").css("font-size",Css.content.fontSize);
        }
        if(Css.content.fontWeight!=null){
             $(".popover-wt-content").css("font-weight",Css.content.fontWeight);
        }
        if(Css.content.paddingT!=null){
             $(".popover-wt-content").css("padding-top",Css.content.paddingT);
        }
        if(Css.content.paddingL!=null){
             $(".popover-wt-content").css("padding-left",Css.content.paddingL);
        }
        if(Css.content.paddingB!=null){
             $(".popover-wt-content").css("padding-bottom",Css.content.paddingB);
        }
        if(Css.content.paddingR!=null){
             $(".popover-wt-content").css("padding-right",Css.content.paddingR);
        }
        if(Css.content.marginT!=null){
             $(".popover-wt-content").css("margin-top",Css.content.marginT);
        }
        if(Css.content.marginL!=null){
             $(".popover-wt-content").css("margin-left",Css.content.marginL);
        }
        if(Css.content.marginB!=null){
             $(".popover-wt-content").css("margin-bottom",Css.content.marginB);
        }
        if(Css.content.marginR!=null){
             $(".popover-wt-content").css("margin-right",Css.content.marginR);
        }
    }
    //Progress Bar CSS
    if(Css.progressbar.hasProperties){
        if(Css.progressbar.backgroundcolor!=null){
            $(".progress-wt-bar").css("background-color",Css.progressbar.backgroundcolor);
            $(".progress-wt-bar").css("border-color",Css.progressbar.backgroundcolor);
        } 
        if(Css.progressbar.color!=null){
             $(".progress-wt-bar").css("color",Css.progressbar.color);
        }
        if(Css.progressbar.hover!=null){
             $('head').append("<style id='custom-style'>:root .popover-wt .progress-wt-bar:hover{background-color:" + Css.progressbar.hover + "!important;}</style>");
        }
        if(Css.progressbar.borderRadius!=null){
             $(".progress-wt-bar").css("border-radius",Css.progressbar.borderRadius);
        }
        if(Css.progressbar.font!=null){
             $(".progress-wt-bar").css("font-family",Css.progressbar.font);
        }
        if(Css.progressbar.fontSize!=null){
             $(".progress-wt-bar").css("font-size",Css.progressbar.fontSize);
        }
        if(Css.progressbar.fontWeight!=null){
             $(".progress-wt-bar").css("font-weight",Css.progressbar.fontWeight);
        }
        if(Css.progressbar.paddingT!=null){
             $(".progress-wt-bar").css("padding-top",Css.progressbar.paddingT);
        }
        if(Css.progressbar.paddingL!=null){
             $(".progress-wt-bar").css("padding-left",Css.progressbar.paddingL);
        }
        if(Css.progressbar.paddingB!=null){
             $(".progress-wt-bar").css("padding-bottom",Css.progressbar.paddingB);
        }
        if(Css.progressbar.paddingR!=null){
             $(".progress-wt-bar").css("padding-right",Css.progressbar.paddingR);
        }
        if(Css.progressbar.marginT!=null){
             $(".progress-wt-bar").css("margin-top",Css.progressbar.marginT);
        }
        if(Css.progressbar.marginL!=null){
             $(".progress-wt-bar").css("margin-left",Css.progressbar.marginL);
        }
        if(Css.progressbar.marginB!=null){
             $(".progress-wt-bar").css("margin-bottom",Css.progressbar.marginB);
        }
        if(Css.progressbar.marginR!=null){
             $(".progress-wt-bar").css("margin-right",Css.progressbar.marginR);
        }
    }
    
}


var popoverHTMLObject = {};
// popoverHTMLObject = {
//     template:'<div class="popover-wt" role="tooltip"><div class="arrow"></div></div>',
//     headerWrapper : '<div class="heads-wt"></div>',
//     stepinfo: '<h3 class="popover-wt-title"></h3>',
//     closeButton :'<button type="button" class="close-wt" data-role="end">&times;</button>',
//     minimizeBtn :null,
//     heading:'<div class="popover-wt-heading"></div>',
//     content :'<div class="popover-wt-content"></div>', 
//     progressbar:'<div class="progress-wt"><div class="progress-wt-bar" role="progressbar" aria-valuenow="70"aria-valuemin="0" aria-valuemax="100" style="width:70%">\
//          <span class="sr-only">70% Complete</span>\
//          </div></div>',
//     navigationWrapper: '<div class="popover-wt-navigation"> <div class="btn-wt-group"></div> </div>',   
//     gotoBtn :"<input type='number' id='step_number' style ='width:60px;height:30px' placeholder='Step #' data-toggle='popup' title='select step' min='1' max='|' /><button class ='btn-wt btn-wlk btn-dafault-wlk next' id='jumpbutton' disabled='true'>Go To</button>",
//     BtnPrev :'<button class="btn-wt btn-wt-sm btn-wt-default back" data-role="none">&laquo; Prev</button> ',
//     BtnNext:'<button class="btn-wt btn-wt-sm btn-wt-default next" data-role="none">Next &raquo;</button>',
//     BtnEnd:'<button class="btn-wt btn-wt-sm btn-wt-default end" data-role="none">End tour</button>',
//     walkthruLogo:null,
//      alignStepInfoText :"right",
//     stepNoInput:null,
//     logoImage :"<li><p style='font-size: 7px;font-weight:700;color: #555555; margin:10px 0px 0px 0px; font-family:Roboto,sans-serif;'>POWERED BY<img src='|' width='35px' style='margin-top:-3px;width:35px!important;'></p></li>", 
//     popovertitletext:'/',
//     endOnClickingOutside:false,
//     buttonPosition:2,

// }

$("#stepinfo-options-wt").on('change',function(){
    if($(this).val()=="default"){
        if($(".popover-wt-title").length>0){
            $(".popover-wt-title").text("Step 1/1");
            popoverHTMLObject.titleNumberAndText = false;
            popoverHTMLObject.popovertitletext = "/";
        }
        else if($(".popover-wt-title").length>0){
             $(".popover-wt-title").text("Step 1/1");
            popoverHTMLObject.titleNumberAndText = false;
            popoverHTMLObject.popovertitletext = "/";
        }
    }
     if($(this).val()=="/"){
        if($(".popover-wt-title").length>0){
            $(".popover-wt-title").text("1/1");
            popoverHTMLObject.titleNumberAndText = true;
            popoverHTMLObject.popovertitletext = "/";
        }
        else if($(".popover-wt-title").length>0){
             $(".popover-wt-title").text("1/1");
            popoverHTMLObject.titleNumberAndText = false;
            popoverHTMLObject.popovertitletext = "/";
        }
    }
     if($(this).val()=="of"){
        if($(".popover-wt-title").length>0){
            $(".popover-wt-title").text("1 of 1");
            popoverHTMLObject.titleNumberAndText = true;
            popoverHTMLObject.popovertitletext = " of ";
        }
        else if($(".popover-wt-title").length>0){
             $(".popover-wt-title").text("1/1");
            popoverHTMLObject.titleNumberAndText = true;
            popoverHTMLObject.popovertitletext = " of ";
        }
    }
})

var popoverAlignments = {

}
var walkthruLogo;

var buttons;
var template;
var imgSrc = "/images/"+popoverHTMLObject.logo;

function walkthruLogoString(){
    if(popoverHTMLObject.logoImage){
    var x  = popoverHTMLObject.logoImage.split('|');
    return x[0]+imgSrc+x[1];
}
else{
    return '';
}
}
$("#show-checkpoint").on("click",function(){
    checkpoint();
   
})
$("#save-checkpoint").on('click',function(){
    $("#status-bar").html("<input type='text' id='checkpoint-name'> &nbsp; <button id='save-cp'>Save</button> ");
    $("#save-cp").on('click',function(){
        var name = $("#checkpoint-name").val();
        if(name.length<2) return false;
        else
        saveCheckpoint(name);
        $("#status-bar").html("CheckPoint Saved: " + name);
    });
    
})
$("#active-popover").on("click",function(){
    activepopover();
    $("#status-bar").html("Your Active Popover");
})
$(".behavior-wt").on("change",function(){
    popover.endOnClickingOutside = $(this).val();
});

activepopover();

function activepopover() {

    var loggeduser = JSON.parse(window.localStorage.getItem('authUser'));
    var tempId = window.localStorage.getItem("TemplateId");
    var user = {};
    user.userId = loggeduser.Id;

    user.token = window.localStorage.getItem('token');
    $.ajax({
        url: api + "epc/activepopover",
        data: { companyId: loggeduser.Company.CompanyId },
        type: "get",
        dataType: "json",
        headers: {
            xaccesstoken: user.token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function (data) {
          window.localStorage.setItem("TemplateId", data.TemplateId);
          Css = JSON.parse(data.CssObject);
          stepbackarr.push(JSON.stringify(Css));
          if(data.PopoverHTMLObject.logo!=undefined)
          imgSrc = "/images/"+data.PopoverHTMLObject.logo;
            //jQuery('head').append(data.CSS);
               popoverHTMLObject = data.PopoverHTMLObject;
          
               popoverHTMLObject.BtnPrev = popoverHTMLObject.BtnPrev.replace('data-role="prev"',"data-role='none'")
       
       
            popoverHTMLObject.BtnNext = popoverHTMLObject.BtnNext .replace('data-role="next"',"data-role='none'")
       
            if(popoverHTMLObject.BtnEnd)
                popoverHTMLObject.BtnEnd =  popoverHTMLObject.BtnEnd.replace('data-role="end"',"data-role='none'")
            else
                popoverHTMLObject.BtnEnd="";

            var buttons = popoverHTMLObject.BtnPrev + popoverHTMLObject.BtnNext + popoverHTMLObject.BtnEnd + walkthruLogoString();
            var navigation = $(popoverHTMLObject.navigationWrapper).html(buttons).prop('outerHTML');
            var header = popoverHTMLObject.stepinfo + popoverHTMLObject.closeButton;
             header = $(popoverHTMLObject.headerWrapper).html(header).prop('outerHTML');
            template = '<div class="popover-wt" role="tooltip"><div class="arrow"></div>'+header+popoverHTMLObject.heading+popoverHTMLObject.content+popoverHTMLObject.progressbar+navigation+'</div>',
            $(template).find(".btn-wt-group").html(buttons)
            runMasterTemplateDefault();
        }
    });
}
function checkpoint() {

    var loggeduser = JSON.parse(window.localStorage.getItem('authUser'));
    var tempId = window.localStorage.getItem("TemplateId");
    var user = {};
    user.userId = loggeduser.Id;

    user.token = window.localStorage.getItem('token');
    $.ajax({
        url: api + "epc/checkpoint",
        data: { companyId: loggeduser.Company.CompanyId },
        type: "get",
        dataType: "json",
        headers: {
            xaccesstoken: user.token
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function (data) {
            window.localStorage.setItem("CheckpointId", data.CheckpointId);
          Css = data.CssObject;
          stepbackarr.push(JSON.stringify(Css));
            //jQuery('head').append(data.CSS);
            if(data.PopoverHTMLObject.logo!=undefined)
          imgSrc = "/images/"+data.PopoverHTMLObject.logo;
          popoverHTMLObject = data.PopoverHTMLObject;
           $("#status-bar").html("Checkpoint:"+ data.CheckpointName);
            if(popoverHTMLObject.buttonPosition==1)
            buttons = "<ul class='navig-btns'>" +walkthruLogoString()+ popoverHTMLObject.BtnOne + popoverHTMLObject.BtnTwo + popoverHTMLObject.BtnThree + "</ul>";
            else if(popoverHTMLObject.buttonPosition==2)
            buttons = "<ul class='navig-btns'>" + popoverHTMLObject.BtnOne + popoverHTMLObject.BtnThree + popoverHTMLObject.BtnTwo +walkthruLogoString()+ "</ul>";
               else if(popoverHTMLObject.buttonPosition==3)
            buttons = "<ul class='navig-btns'>" + walkthruLogoString()+popoverHTMLObject.BtnOne  +popoverHTMLObject.BtnTwo+  popoverHTMLObject.BtnThree+ "</ul>";
            var cb = popoverHTMLObject.isCloseButton?popoverHTMLObject.closeButton:''
            template =  cb + "<div class='wt-top-s-1'><div class='wt-top-s-1-col-1'><p class='popover-wt-title'></p></div></div><div class='wt-middle-s-2 popover-content-wt'> </div><div class='popover-navigation-wt'>" + buttons + "</div> ";
            
            runMasterTemplateDefault();
        }
    });
}
$("#reset-css").on('click',function(){
    Css = {
    popover: { hasProperties: false, backgroundcolor: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    nextButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    endButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    backButton: { hasProperties: false, backgroundcolor: null, color: null, hover: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    closeButton: { hasProperties: false, backgroundcolor: null, color: null, borderRadius: null, borderColor: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    stepsTitle: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    heading: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    content: { hasProperties: false, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null },
    progressbar: { hasProperties: false, backgroundcolor: null, color: null, fontWeight: null, fontSize: null, font: null, paddingT: null, paddingL: null, paddingB: null, paddingR: null, marginT: null, marginL: null, marginB: null, marginR: null }

};
    $("#popover-custom-wt").html(" ");
    $("#popover-custom-wt").remove();

})
  var currentTour;

function initializeTemplate(templateId) {
    if(currentTour)
    currentTour.end();
    //ajax request to template Id 
    //initialize variables
    var temp = { "templateId": templateId };
    jQuery.ajax({
        url: api + "epc/",
        type: "GET",
        data: temp,
        headers: {
            xaccesstoken: data.token,
        },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            window.localStorage.setItem("MasterTemplateId", data.TemplateId);
            window.localStorage.setItem("TemplateId", data.TemplateId);
          Css = JSON.parse(data.CssObject);
          stepbackarr.push(JSON.stringify(Css));
          if(data.PopoverHTMLObject.logo!=undefined)
          imgSrc = "/images/"+data.PopoverHTMLObject.logo;
            //jQuery('head').append(data.CSS);
               popoverHTMLObject = data.PopoverHTMLObject;
          
               popoverHTMLObject.BtnPrev = popoverHTMLObject.BtnPrev.replace("data-role='prev'","data-role='none'")
       
       
            popoverHTMLObject.BtnNext = popoverHTMLObject.BtnNext.replace("data-role='next'","data-role='none'")
       
            if(popoverHTMLObject.BtnEnd)
                popoverHTMLObject.BtnEnd =  popoverHTMLObject.BtnEnd.replace("data-role='end'","data-role='none'")
            else
                popoverHTMLObject.BtnEnd="";

            var buttons = popoverHTMLObject.BtnPrev + popoverHTMLObject.BtnNext + popoverHTMLObject.BtnEnd + walkthruLogoString();
            var navigation = $(popoverHTMLObject.navigationWrapper).html(buttons).prop('outerHTML');
            var header = popoverHTMLObject.stepinfo + popoverHTMLObject.closeButton;
             header = $(popoverHTMLObject.headerWrapper).html(header).prop('outerHTML');
            template = '<div class="popover-wt" role="tooltip"><div class="arrow"></div>'+header+popoverHTMLObject.heading+popoverHTMLObject.content+popoverHTMLObject.progressbar+navigation+'</div>',
            $(template).find(".btn-wt-group").html(buttons)
            runMasterTemplateDefault();
        }
    });
}

function applyCSS() {
    var css = '<style>';
    css += ':root .navig-btns>li{ float:right!important;}';
    $('head').append(css);
}

function initializeProgressbar(percent) {
    var pbar = progressbar.split('|');

    var str = pbar[0]; + percent + pbar[1] + percent + pbar[2] + percent + pbar[3];
    return str;
}
function addGotoInputButton(steps) {
    var arr = popoverHTMLObject.gotoBtn.split('|');
    return (arr[0] + steps + arr[1]);

}
//Save Walkthru Button Handler
$('#save-wt').on('click', function (e) {
    e.stopImmediatePropagation();
    savePopover();
     $("#status-bar").html("Changes Published");
    
})
$('#logo-color-wt').on('change', function (e) {
    e.stopImmediatePropagation();
    Css.logoText = {color:null};
    var src ="/images/"+$(this).val();
    $("#wlk-logo-wt").attr("src",src);
    if(src.indexOf("white")!=-1){
        Css.logoText.color = "white";
         $("#wlk-logo-wt").parent().css("color","white");
    }
   
     if(src.indexOf("grey")!=-1){
         Css.logoText.color = "black";
         $("#wlk-logo-wt").parent().css("color","black");
     }
   
    popoverHTMLObject.logo = $(this).val();
    Css.logoText.hasProperties = true;

    
})



function setMasterPopUpTemplate(tour_steps) {
    var progressbar = '';
    var gotobtn = '';
    $.each(tour_steps, function (i, step) {
        //Case 1 when heading is aligned and Custom title Text is defined and no close button 
        if (popoverHTMLObject.isAlignedHeading) {
            
            var stepInfo = popoverHTMLObject.isStepInfo?step['title'].replace('Step ', '').replace('/', '') + popoverHTMLObject.popovertitletext + tour_steps.length:'';
            var left = "<div style='float:left' class='popover-wt-heading'>" + step['heading'] + "</div>";
            var right = "<div style='float:right' class='popover-wt-title'>" + stepInfo + "</div>";
            debugger;
            step['title'] = left + right;
        }
        //Case 2 when heading is not aligned but custom title text is defined and aligned left
        else if (popoverHTMLObject.titleNumberAndText) {
             var stepInfo = popoverHTMLObject.isStepInfo?step['title'].replace('Step ', '').replace('/', '') + popoverHTMLObject.popovertitletext + tour_steps.length:'';
            var right = "<div class='popover-wt-title' style='float:" + popoverHTMLObject.alignStepInfoText + "'>" + stepInfo + "</div>";
            step['title'] = right;
        }
        else {
            popoverHTMLObject.isStepInfo?step['title'] += popoverHTMLObject.popovertitletext + tour_steps.length: step['title']='';
            
        }
        if (popoverHTMLObject.isProgressbar) {
            var percent = parseInt(((i + 1) / tour_steps.length) * 100);
            var pb = '<div class="pbar_wrapper"><div class="progress-wt"><div class="progress-wt-bar progress-bar-success-wt" role="progressbar" aria-valuenow="' + percent + '" aria-valuemin="0" aria-valuemax="100" style="background-color: #84e297;width:' + percent + '%;">' + percent + '%</div></div>';
            progressbar = popoverHTMLObject.isProgressbar ? pb : '';
        }
        if (popoverHTMLObject.isStepGoto) {

            gotobtn = popoverHTMLObject.isStepGoto ? addGotoInputButton(tour_steps.length) : '';
        }


        if (popoverHTMLObject.isHeading && !popoverHTMLObject.isAlignedHeading) {
            step['content'] = '<div class="popover-wt-heading" style="margin-top:17px;">' + step['heading'] + '</div><div class="popover-wt-content">' + step['content'] + '</div>' + progressbar + gotobtn + '</div>';
        }
        else {
            step['content'] = '<div class="popover-wt-content">' + step['content'] + '</div>' + progressbar + gotobtn + '</div>';
        }
    });

}





function runMasterTemplateDefault() {
    var requestByPage = {};
    walkthroughObj = [];
    var def_steps = 2;
    var def_placement = "left";
    var item = {};
    item["element"] = "#parent";
    item["title"] = "Step 1";
    item["heading"] = "Heading in popover";
    item["content"] = "Content For walkthru Description";
    item["sno"] = 7;
    item["type"] = "text";
    item["placement"] = "bottom";
    item["backdrop"] = false;
    item["duration"] = 0;
    item["delay"] = false;
    item["ElementId"] = "4db977b8-5cbd-4385-a728-c8638131b3a5";
    item["StepId"] = "88c6f51c-c75e-40ed-9966-b21590edfc46";
    item["Orphan"] = true;
    item["progress"] = 0.5;
    walkthroughObj.push(item);
   // setMasterPopUpTemplate(walkthroughObj);
    //getting all pages
    masterTemplateTour(walkthroughObj, "05c8d6c1-bc36-47ed-a7bd-5ee630dc30ab", false, window.location.host + window.location.pathname);

}


function masterTemplateTour(stepsArr, name, backdrop, forSite) {

    var tour = new Tour({
        container: "body",
        name: "Testing Tour Popover",
        keyboard: true,
        autoscroll: true,
        debug: true,
        backdrop: backdrop,
        backdropContainer: 'body',
        steps: stepsArr,
        template:  template,
        afterGetState: function (key, value) { },
        afterSetState: function (key, value) { },
        afterRemoveState: function (key, value) { },
        onStart: function (tour) {
        },
        onEnd: function (tour) {


        },
        onShow: function (tour) {

        },
        onShown: function (tour) {
          
           
            ParseTempCss();
            changeText();
           
            $('.popover-wt').on('click', function (e) {
                globalSelector = e.target;
                showComponenCss();
                if (globalSelector.className.indexOf('popover-wt') != -1) {
                    $("#component-settings").html("Popover Settings")
                }
                if (globalSelector.className.indexOf('popover-wt-title') != -1) {
                    $("#component-settings").html("Step Info Settings")
                }
                if (globalSelector.className.indexOf('popover-wt-heading') != -1 || globalSelector.className.indexOf('popover-wt-heading') != -1) {
                    $("#component-settings").html("Heading Settings")
                }
                if (globalSelector.className.indexOf('popover-wt-content') != -1) {
                    $("#component-settings").html("Content Settings")
                }
                if (globalSelector.className.indexOf('next') != -1) {
                
                    $("#component-settings").html("Next Button Settings")
                }
                if (globalSelector.className.indexOf('end') != -1) {

                    $("#component-settings").html("End Button Settings")

                }
                if (globalSelector.className.indexOf('back') != -1) {

                    $("#component-settings").html("Back Button Settings")
                }
                if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
                    $("#component-settings").html("Progress-Bar Settings")
                }
                if (globalSelector.className.indexOf('close-wt') != -1) {
                    $("#component-settings").html("Close Button Settings")
                }

            })
            $('.popover-wt').on('mouseover', function (e) {

                inspectorMouseOver(e);

            })
            $('.popover-wt').on('mouseout', function (e) {
                inspectorMouseOut(e);
            })
            $( ".popover-wt" ).resizable();


        },
        onHide: function (tour) { },
        onHidden: function (tour) { },
        onNext: function (tour) {

        },
        onPrev: function (tour) {

        },
        onPause: function (tour, duration) { },
        onResume: function (tour, duration) { },
        onRedirectError: function (tour) { }
    });
    tour._options.steps = [];
    tour.addSteps(stepsArr);
    tour.init(true);
    tour.start(true);
    currentTour = tour;
}


function changearrowColor(color) {
    var css = '<style id="arrow-css-wt">:root .popover-wt.bottom>.arrow:after{border-bottom-color:' + color + '}\
	:root .popover-wt.top>.arrow:after{border-top-color:'+ color + '}\
	:root .popover-wt.left>.arrow:after{border-left-color:'+ color + '}\
	:root .popover-wt.right>.arrow:after{border-right-color:'+ color + '}\
	</style>';
    $('head').append(css);

    $('.popover-wt.top>.arrow').css('border-top-color', color);
    $('.popover-wt.right>.arrow ').css('border-right-color', color);
    $('.popover-wt.bottom>.arrow ').css('border-bottom-color', color);
    $('.popover-wt.left>.arrow ').css('border-left-color', color);


}
var last = '';

function getContextElement($this) {
    var color = $('#chosen-value').val();
    console.log($this + '#' + color);
}


function inspectorMouseOut(e) {
    // Remove outline from element:

    $(e.target).css('outline', 'none');

}

function inspectorMouseOver(e) {
    // NB: this doesn't work in IE (needs fix):
    element = e.target;
    $(e.target).css('outline', '2px solid grey');

    // Set last selected element so it can be 'deselected' on cancel.
    last = element;
}


elementControl();

function changeColor(selectedElem) {
    return (function (color) {
        $(selectedElem).css('background-color', '#' + color);
    }

    );
}

//change text color
$('#chosen-value-text').on('change', function () {
    var color = '#' + $(this).val();

    if (globalSelector.className.indexOf('popover-wt-title') != -1) {

        Css.stepsTitle.hasProperties = true;
        Css.stepsTitle.color = color;
    }
    if (globalSelector.className.indexOf('popover-wt-heading')!= -1 ) {
        Css.heading.hasProperties = true;
        Css.heading.color = color;
    }
    if (globalSelector.className.indexOf('popover-wt-content') != -1) {
        Css.content.hasProperties = true;
        Css.content.color = color;
    }
     if (globalSelector.className.indexOf('close-wt') != -1) {
        Css.closeButton.hasProperties = true;
        Css.closeButton.color = color;
    }
     if (globalSelector.className.indexOf('popover-wt-title') != -1) {
        Css.stepsTitle.hasProperties = true;
        Css.stepsTitle.color = color;
    }
    if (globalSelector.className.indexOf('next') != -1) {
        Css.nextButton.hasProperties = true;
        Css.nextButton.color = color;
    }
    if (globalSelector.className.indexOf('end') != -1) {
        Css.endButton.hasProperties = true;
        Css.endButton.color = color;
    }
    if (globalSelector.className.indexOf('back') != -1) {
        Css.backButton.hasProperties = true;
        Css.backButton.color = color;
    }
    if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
        Css.progressbar.hasProperties = true;
        Css.progressbar.color = color;
    }
    $(globalSelector).css('color', color);
    if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));

});
//background color changer
$('#chosen-value').on('change', function () {
    var color = '#' + $(this).val();
    if ($(globalSelector).hasClass('popover-wt')) {
        changearrowColor(color);
        Css.popover.hasProperties = true;
        Css.popover.backgroundcolor = color;
        $(globalSelector).css('background-color', color);
       

    }
    if (globalSelector.className.indexOf('close-wt') != -1) {
        Css.closeButton.hasProperties = true;
        Css.closeButton.backgroundcolor = color;
        $(globalSelector).css('background-color', color);
    }

    if (globalSelector.className.indexOf('next') != -1) {
        Css.nextButton.hasProperties = true;
        Css.nextButton.backgroundcolor = color;
        $(globalSelector).css('background-color', color);
    }
    if (globalSelector.className.indexOf('end') != -1) {
        Css.endButton.hasProperties = true;
        Css.endButton.backgroundcolor = color;
        $(globalSelector).css('background-color', color);

    }
    if (globalSelector.className.indexOf('back') != -1) {
        Css.backButton.hasProperties = true;
        Css.backButton.backgroundcolor = color;
        $(globalSelector).css('background-color', color);
    }
    if ($(globalSelector).hasClass('progress-wt-bar')) {
        Css.progressbar.hasProperties = true;
        Css.progressbar.backgroundcolor = color;
        $(globalSelector).css('background-color', color);
    }
    
    if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    
});





function elementControl($this) {

    var colorPicker = " <button class='pickercontrol' style='border: 1px solid;' >Background Color</button><label class='hex' >HEX</label> <input  id='chosen-value' value='000000'>";
    var backgroundcolor = 'Color: <input class="jscolor" value="ab2567">';
    var color = '';
    $('#controls').append(colorPicker);
    $('.pickercontrol').attr('class', "jscolor {valueElement:'chosen-value', onFineChange:''} ")
    colorPicker = "<button class='pickercontrol2' style='width: 126px; border: 1px solid;'>Text Color</button><label class='hex'>HEX</label> <input  id='chosen-value-text' value='000000'>";
    backgroundcolor = 'Color: <input class="jscolor" value="ab2567">';
    color = '';
    $('#controls').append(colorPicker);
    $('.pickercontrol2').attr('class', "jscolor {valueElement:'chosen-value-text', onFineChange:''} ")

}
function marginControls() {
    var margin = "<hr><div class='container-fluid margin-wt'><div class='row'><h5><label>Margin</label></h5><div class='col-md-3'><div><label>top</label><input  type='number' value='' id='mt-wt'></div></div><div class='col-md-3'><div><label>left</label><input id='ml-wt' type='number' value='' ></div></div><div class='col-md-3'><div ><label>bottom</label><input id='mb-wt' type='number' value=''></div></div><div class='col-md-3'><div><label>right</label><input id='mr-wt' type='number' value='' ></div></div></div></div>";
    $('#controls').append(margin);
}
function paddingControls() {
    var padding = "<hr><div class='container-fluid margin-wt'><div class='row'><h5><label>Padding</label></h5><div class='col-md-3'><div><label>top</label><input  type='number' value='' id='pt-wt'></div></div><div class='col-md-3'><div><label>left</label><input id='pl-wt' type='number' value='' ></div></div><div class='col-md-3'><div><label>bottom</label><input id='pb-wt' type='number' value='' ></div></div><div class='col-md-3'><div><label>right</label><input id='pr-wt' type='number' value='' ></div></div></div></div>";
    $('#controls').append(padding);
}

HoverControls();
$('#controls').append("<hr>");
changeTextControls();
FontControls();
$('#controls').append("<hr>");

changeFontSize();
changeFont();
changeFontWeight();
function FontControls() {
    var inp = "<div id='font-wrapper'><label>Font Size</label> <input type='number' id='font-size' />"
    $('#controls').append(inp);
    var family = "<div id='font-wrapper'><label>Font Family</label><select id='font-family'><option value='serif'>Default</option><option value='sans-serif'>Sans-Serif</option><option value='monospace'>Monospace</option><option value='cursive'>Cursive</option><option value='fantasy'>Fantasy</option><option value='WhitneyMedium'>WhitneyMedium</option><option value='Asap Condensed'>Asap Condensed</option><option value='Noto Sans'>Noto Sans</option><option value='Roboto'>Roboto</option><option value='Lato'>Lato</option><option value='Open Sans'>Open Sans</option><option value='Slabo 27px'>Slabo 27px</option><option value='Roboto Condensed'>Roboto Condensed</option><option value='Raleway'>Raleway</option><option value='PT Sans'>PT Sans</option><option value='Ubuntu'>Ubuntu</option><option value='Roboto Slab'>Roboto Slab</option><option value='PT Serif'>PT Serif</option><option value='Saira'>Saira</option><option value='Arimo'>Arimo</option><option value='PT Sans Narrow'>PT Sans Narrow</option></select></div>";
    $('#controls').append(family);
    var fontWeight = "<div id='font-wrapper'><label>Font Weight</label><select id='font-weight'><option value='100'>100</option><option value='200'>200</option>\
    <option value='300'>300</option>\
    <option value='400'>400</option>\
    <option value='500'>500</option>\
    <option value='600'>600</option>\
    <option value='700'>700</option>\
    <option value='800'>800</option>\
    </select></div>";
    $('#controls').append(fontWeight);
}



function changeFont() {
    $('#font-family').on('change', function () {
        var size = $(this).val();

        if (globalSelector.className.indexOf('popover-wt-title') != -1) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.font = size;
        }
         if (globalSelector.className.indexOf('popover-wt-heading') != -1 || globalSelector.className.indexOf('popover-wt-heading') != -1) {
            Css.heading.hasProperties = true;
            Css.heading.font = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.font = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.font = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.font = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.font = size;
        }
        $(globalSelector).css('font-family', $(this).val());
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })

}
function changeFontWeight() {
    $('#font-weight').on('change', function () {
        var size = $(this).val();
        if (globalSelector.className.indexOf('popover-wt-title') != -1) {
            Css.StepsTitle.hasProperties = true;
            Css.stepsTitle.fontWeight = size;
        }
         if (globalSelector.className.indexOf('popover-wt-heading') != -1 || globalSelector.className.indexOf('popover-wt-heading') != -1) {
            Css.heading.hasProperties = true;
            Css.heading.fontWeight = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.fontWeight = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.fontWeight = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.fontWeight = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.fontWeight = size;
        }
        $(globalSelector).css('font-weight', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })

}

function changeFontSize() {
    $('#font-size').on('input keyup', function () {
        console.log("fontsize changes")

        var size = $(this).val() + "px";
        if (globalSelector.className.indexOf('popover-wt-title') != -1 || globalSelector.className.indexOf('popover-wt-title') != -1 ) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.fontSize = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading') != -1 || globalSelector.className.indexOf('popover-wt-heading') != -1) {
            Css.heading.hasProperties = true;
            Css.heading.fontSize = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.fontSize = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.fontSize = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.fontSize = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.fontSize = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.fontSize = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.fontSize = size;
        }

        $(globalSelector).css('font-size', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })

}
BorderControls();
marginControls()
paddingControls()
changeMargins()
changePaddings()

$('#controls').append("<hr>");
changeBorderRadius();

function BorderControls() {
    var inp = "<div id='font-wrapper'><label>Border-Radius</label> <input type='number' id='border-radius' />"
    $('#controls').append(inp);
}
function HoverControls() {
    var colorPicker = "<div class='hover-wt'> <button class='hover-color' style='width: 126px; border: 1px solid;' >Hover Color</button><label class='hex' style='margin-left:64px;'>HEX</label> <input  id='hover-color' value='000000'></div>";
    var backgroundcolor = 'Color: <input class="jscolor" value="ab2567">';
    var color = '';
    $('#controls').append(colorPicker);
    $('.hover-color').attr('class', "jscolor {valueElement:'hover-color', onFineChange:''} ")

}
function changeBorderRadius() {
    $('#border-radius').on('change', function () {


        var size = $(this).val() + "px";
        if (globalSelector.className.indexOf('popover-wt') != -1) {

            Css.popover.borderRadius = size;

        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.borderRadius = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.borderRadius = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.borderRadius = size;
        }

        $(globalSelector).css('border-radius', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })

}

$('#hover-color').on('change', function () {
    var color = '#' + $(this).val();


    if (globalSelector.className.indexOf('next') != -1) {

        Css.nextButton.hasProperties = true;
        Css.nextButton.hover = color;
        $('head').append("<style id='custom-style'>:root .popover-wt .next:hover{background-color:" + color + "!important;}</style>");

    }
    if (globalSelector.className.indexOf('back') != -1) {
        Css.backButton.hasProperties = true;
        Css.backButton.hover = color;
        $('head').append("<style id='custom-style'>:root .popover-wt .back:hover{background-color:" + color + "!important;}</style>");

    }
    if (globalSelector.className.indexOf('end') != -1) {
        Css.endButton.hasProperties = true;
        Css.endButton.hover = color;
        $('head').append("<style id='custom-style'>:root .popover-wt .end:hover{background-color:" + color + "!important;}</style>");

    }

    if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
});

function savePopover() {
    var loggeduser = JSON.parse(window.localStorage.getItem('authUser'));
    var tempId = window.localStorage.getItem("TemplateId");
    var data = {};
    data.userId = loggeduser.Id;

    data.token = window.localStorage.getItem('token');
    var style = parseCssObject();
    
       
            popoverHTMLObject.BtnPrev = popoverHTMLObject.BtnPrev.replace('data-role="none"',"data-role='prev'")
       
       
            popoverHTMLObject.BtnNext = popoverHTMLObject.BtnNext.replace('data-role="none"',"data-role='next'")
       
            if(!popoverHTMLObject.BtnEnd)
            popoverHTMLObject.BtnEnd = popoverHTMLObject.BtnEnd.replace('data-role="none"',"data-role='end'");
    
     
    
    var popoverdata = {
        "TemplateId": tempId ? tempId : '',
        "PopoverHTMLObject": JSON.stringify(popoverHTMLObject),
        "CompanyId": loggeduser.Company.CompanyId,
        "CSS": style,
        "CssObject": JSON.stringify(Css),

    };

    jQuery.ajax({
        url: api + "epc/savetemplate",
        type: "POST",
        data: popoverdata,
        headers: {
            xaccesstoken: data.token,
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function (data) {
            if (data.updated) {
                
            }
            else {
                window.localStorage.setItem("TemplateId", data.TemplateId);
            }
        }
    })
}

function parseCssObject() {
    var cssString = "<style id='popover-custom-wt'>"
    //for popover background-customizations
    if (Css.popover.hasProperties) {
        var color = Css.popover.backgroundcolor;
        var arrow = ':root .popover-wt.bottom>.arrow:after{border-bottom-color:' + color + '}\
	:root .popover-wt.top>.arrow:after{border-top-color:'+ color + '}\
	:root .popover-wt.left>.arrow:after{border-left-color:'+ color + '}\
	:root .popover-wt.right>.arrow:after{border-right-color:'+ color + '}';

        cssString += ":root .popover-wt{background-color:" + color + "!important;";
        if (Css.popover.borderRadius) {
            cssString += "background-color:" + color + "!important;";
        }
        if (Css.popover.paddingT) {
            cssString += "padding-top:" + Css.popover.paddingT + "!important;";
        }
        if (Css.popover.paddingL) {
            cssString += "padding-left:" + Css.popover.paddingT + "!important;";
        }
        if (Css.popover.paddingB) {
            cssString += "padding-bottom:" + Css.popover.paddingT + "!important;";
        }
        if (Css.popover.paddingR) {
            cssString += "padding-right:" + Css.popover.paddingT + "!important;";
        }
        cssString += "}" + arrow;
    }
    //buttons\
    if (Css.nextButton.hasProperties) {
        cssString += ":root .popover-wt .next{";

        if (Css.nextButton.backgroundcolor) {
            var bg = Css.nextButton.backgroundcolor;
            cssString += "background-color:" + bg + "!important;border-color:" + bg + ";";
        }
        if (Css.nextButton.borderRadius) {
            cssString += 'border-radius:' + Css.nextButton.borderRadius + ';';
        }
        if (Css.nextButton.color) {
            cssString += 'color:' + Css.nextButton.color + ';';
        }
        if (Css.nextButton.font) {
            cssString += 'font:' + Css.nextButton.font + ';';
        }
        if (Css.nextButton.fontSize) {
            cssString += 'font-size:' + Css.nextButton.fontSize + ';';
        }
        if (Css.nextButton.fontWeight) {
            cssString += 'font-weight:' + Css.nextButton.fontWeight + ';';
        }
        if (Css.nextButton.marginT) {
            cssString += "margin-top:" + Css.nextButton.marginT + "!important;";
        }
        if (Css.nextButton.marginL) {
            cssString += "margin-left:" + Css.nextButton.marginL + "!important;";
        }
        if (Css.nextButton.marginB) {
            cssString += "margin-bottom:" + Css.nextButton.marginB + "!important;";
        }
        if (Css.nextButton.marginR) {
            cssString += "margin-right:" + Css.nextButton.marginR + "!important;";
        }
        cssString += '}';
        if (Css.nextButton.hover) {
            cssString += ":root .popover-wt .next:hover{background-color:" + Css.nextButton.hover + "}";
        }
    }
    if (Css.backButton.hasProperties) {
        cssString += ":root .popover-wt .back{";

        if (Css.backButton.backgroundcolor) {
            var bg = Css.backButton.backgroundcolor;
            cssString += "background-color:" + bg + "!important;border-color:" + bg + ";";
        }
        if (Css.backButton.borderRadius) {
            cssString += 'border-radius:' + Css.backButton.borderRadius + ';';
        }
        if (Css.backButton.color) {
            cssString += 'color:' + Css.backButton.color + ';';
        }
        if (Css.backButton.font) {
            cssString += 'font:' + Css.backButton.font + ';';
        }
        if (Css.backButton.fontSize) {
            cssString += 'font-size:' + Css.backButton.fontSize + ';';
        }
        if (Css.backButton.fontWeight) {
            cssString += 'font-weight:' + Css.backButton.fontWeight + ';';
        }
        if (Css.backButton.marginT) {
            cssString += "margin-top:" + Css.backButton.marginT + "!important;";
        }
        if (Css.backButton.marginL) {
            cssString += "margin-left:" + Css.backButton.marginL + "!important;";
        }
        if (Css.backButton.marginB) {
            cssString += "margin-bottom:" + Css.backButton.marginB + "!important;";
        }
        if (Css.backButton.marginR) {
            cssString += "margin-right:" + Css.backButton.marginR + "!important;";
        }
        cssString += '}';
        if (Css.backButton.hover) {
            cssString += ":root .popover-wt .back:hover{background-color:" + Css.backButton.hover + "}";
        }
    }
    if (Css.endButton.hasProperties) {
        cssString += ":root .popover-wt .end{";

        if (Css.endButton.backgroundcolor) {
            var bg = Css.endButton.backgroundcolor;
            cssString += "background-color:" + bg + "!important;border-color:" + bg + "!important;";
        }
        if (Css.endButton.borderRadius) {
            cssString += 'border-radius:' + Css.endButton.borderRadius + ';';
        }
        if (Css.endButton.color) {
            cssString += 'color:' + Css.endButton.color + ';';
        }
        if (Css.endButton.font) {
            cssString += 'font:' + Css.endButton.font + ';';
        }
        if (Css.endButton.fontSize) {
            cssString += 'font-size:' + Css.endButton.fontSize + ';';
        }
        if (Css.endButton.fontWeight) {
            cssString += 'font-weight:' + Css.endButton.fontWeight + ';';
        }
        if (Css.endButton.marginT) {
            cssString += "margin-top:" + Css.endButton.marginT + "!important;";
        }
        if (Css.endButton.marginL) {
            cssString += "margin-left:" + Css.endButton.marginL + "!important;";
        }
        if (Css.endButton.marginB) {
            cssString += "margin-bottom:" + Css.endButton.marginB + "!important;";
        }
        if (Css.endButton.marginR) {
            cssString += "margin-right:" + Css.endButton.marginR + "!important;";
        }
        cssString += '}';
        if (Css.endButton.hover) {
            cssString += ":root .popover-wt .end:hover{background-color:" + Css.endButton.hover + "}";
        }
    }
    if (Css.closeButton.hasProperties) {
        cssString += ":root .popover-wt .close-wt{";

        if (Css.closeButton.backgroundcolor) {
            var bg = Css.closeButton.backgroundcolor;
            cssString += "background-color:" + bg + "!important;border-color:" + bg + "!important;";
        }
        if (Css.closeButton.borderRadius) {
            cssString += 'border-radius:' + Css.closeButton.borderRadius + ';';
        }
        if (Css.closeButton.color) {
            cssString += 'color:' + Css.closeButton.color + ';';
        }
        if (Css.closeButton.font) {
            cssString += 'font:' + Css.closeButton.font + ';';
        }
        if (Css.closeButton.fontSize) {
            cssString += 'font-size:' + Css.closeButton.fontSize + ';';
        }
        if (Css.closeButton.fontWeight) {
            cssString += 'font-weight:' + Css.closeButton.fontWeight + ';';
        }
        if (Css.closeButton.marginT) {
            cssString += "margin-top:" + Css.closeButton.marginT + "!important;";
        }
        if (Css.closeButton.marginL) {
            cssString += "margin-left:" + Css.closeButton.marginL + "!important;";
        }
        if (Css.closeButton.marginB) {
            cssString += "margin-bottom:" + Css.closeButton.marginB + "!important;";
        }
        if (Css.closeButton.marginR) {
            cssString += "margin-right:" + Css.closeButton.marginR + "!important;";
        }
        if (Css.closeButton.paddingT) {
            cssString += "padding-top:" + Css.closeButton.paddingT + "!important;";
        }
        if (Css.closeButton.paddingL) {
            cssString += "padding-left:" + Css.closeButton.paddingL + "!important;";
        }
        if (Css.closeButton.paddingB) {
            cssString += "padding-bottom:" + Css.closeButton.paddingT + "!important;";
        }
        if (Css.closeButton.paddingR) {
            cssString += "padding-right:" + Css.closeButton.paddingT + "!important;";
        }
        cssString += '}';
        if (Css.closeButton.hover) {
            cssString += ":root .popover-wt .end:hover{background-color:" + Css.closeButton.hover + "}";
        }
    }
    if (Css.stepsTitle.hasProperties) {
        cssString += ":root .popover-wt-title{";



        if (Css.stepsTitle.color) {
            cssString += 'color:' + Css.stepsTitle.color + ';';
        }
        if (Css.stepsTitle.font) {
            cssString += 'font:' + Css.stepsTitle.font + ';';
        }
        if (Css.stepsTitle.fontSize) {
            cssString += 'font-size:' + Css.stepsTitle.fontSize + '!important;';
        }
        if (Css.stepsTitle.fontWeight) {
            cssString += 'font-weight:' + Css.stepsTitle.fontWeight + ';';
        }
        if (Css.stepsTitle.marginT) {
            cssString += "margin-top:" + Css.stepsTitle.marginT + "!important;";
        }
        if (Css.stepsTitle.marginL) {
            cssString += "margin-left:" + Css.stepsTitle.marginL + "!important;";
        }
        if (Css.stepsTitle.marginB) {
            cssString += "margin-bottom:" + Css.stepsTitle.marginB + "!important;";
        }
        if (Css.stepsTitle.marginR) {
            cssString += "margin-right:" + Css.stepsTitle.marginR + "!important;";
        }
        if (Css.stepsTitle.paddingT) {
            cssString += "padding-top:" + Css.stepsTitle.paddingT + "!important;";
        }
        if (Css.stepsTitle.paddingL) {
            cssString += "padding-left:" + Css.stepsTitle.paddingT + "!important;";
        }
        if (Css.stepsTitle.paddingB) {
            cssString += "padding-bottom:" + Css.stepsTitle.paddingT + "!important;";
        }
        if (Css.stepsTitle.paddingR) {
            cssString += "padding-right:" + Css.stepsTitle.paddingT + "!important;";
        }
        cssString += '}';

    }
    if (Css.heading.hasProperties) {
        cssString += ":root .popover-wt .popover-wt-text,:root .popover-wt wt-top-s-1,:root .popover-wt .popover-wt-heading{";



        if (Css.heading.color) {
            cssString += 'color:' + Css.heading.color + ';';
        }
        if (Css.heading.font) {
            cssString += 'font:' + Css.heading.font + ';';
        }
        if (Css.heading.fontSize) {
            cssString += 'font-size:' + Css.heading.fontSize + ' !important;';
        }
        if (Css.heading.fontWeight) {
            cssString += 'font-weight:' + Css.heading.fontWeight + ';';
        }
        if (Css.heading.marginT) {
            cssString += "margin-top:" + Css.heading.marginT + "!important;";
        }
        if (Css.heading.marginL) {
            cssString += "margin-left:" + Css.heading.marginL + "!important;";
        }
        if (Css.heading.marginB) {
            cssString += "margin-bottom:" + Css.heading.marginB + "!important;";
        }
        if (Css.heading.marginR) {
            cssString += "margin-right:" + Css.heading.marginR + "!important;";
        }
        if (Css.heading.paddingT) {
            cssString += "padding-top:" + Css.heading.paddingT + "!important;";
        }
        if (Css.heading.paddingL) {
            cssString += "padding-left:" + Css.heading.paddingT + "!important;";
        }
        if (Css.heading.paddingB) {
            cssString += "padding-bottom:" + Css.heading.paddingT + "!important;";
        }
        if (Css.heading.paddingR) {
            cssString += "padding-right:" + Css.heading.paddingT + "!important;";
        }
        cssString += '}';

    }
    if (Css.content.hasProperties) {
        cssString += ":root .popover-wt .popover-wt-content{";

        if (Css.content.color) {
            cssString += 'color:' + Css.content.color + ';';
        }
        if (Css.content.font) {
            cssString += 'font:' + Css.content.font + ';';
        }
        if (Css.content.fontSize) {
            cssString += 'font-size:' + Css.content.fontSize + ';';
        }
        if (Css.content.fontWeight) {
            cssString += 'font-weight:' + Css.content.fontWeight + ';';
        }
        if (Css.content.marginT) {
            cssString += "margin-top:" + Css.content.marginT + "!important;";
        }
        if (Css.content.marginL) {
            cssString += "margin-left:" + Css.content.marginL + "!important;";
        }
        if (Css.content.marginB) {
            cssString += "margin-bottom:" + Css.content.marginB + "!important;";
        }
        if (Css.content.marginR) {
            cssString += "margin-right:" + Css.content.marginR + "!important;";
        }
        if (Css.content.paddingT) {
            cssString += "padding-top:" + Css.content.paddingT + "!important;";
        }
        if (Css.content.paddingL) {
            cssString += "padding-left:" + Css.content.paddingT + "!important;";
        }
        if (Css.content.paddingB) {
            cssString += "padding-bottom:" + Css.content.paddingT + "!important;";
        }
        if (Css.content.paddingR) {
            cssString += "padding-right:" + Css.content.paddingT + "!important;";
        }
        cssString += '}';

    }
    if (Css.progressbar.hasProperties) {
        cssString += ":root .popover-wt .progress-wt-bar{";

        if (Css.progressbar.backgroundcolor) {
            cssString += 'background-color:' + Css.progressbar.backgroundcolor + '!important;';
        }
        if (Css.progressbar.color) {
            cssString += 'color:' + Css.progressbar.color + ';';
        }
        if (Css.progressbar.font) {
            cssString += 'font:' + Css.progressbar.font + ';';
        }
        if (Css.progressbar.fontSize) {
            cssString += 'font-size:' + Css.progressbar.fontSize + ';';
        }
        if (Css.progressbar.fontWeight) {
            cssString += 'font-weight:' + Css.progressbar.fontWeight + ';';
        }
        cssString += '}';

    }
    if(Css.logoText.hasProperties){
        cssString +=":root .popover-wt .navig-btns>li>p{";
        if (Css.logoText.color) {
            cssString += 'color:' + Css.logoText.color + ';';
        }
        cssString += '}';

    }


    cssString += "</style>";

    return cssString;

}

function saveMasterPopover() {
    var loggeduser = JSON.parse(window.localStorage.getItem('authUser'));
    var tempId = window.localStorage.getItem("MasterTemplateId");
    var data = {};
    data.userId = loggeduser.Id;

    data.token = window.localStorage.getItem('token');
    var style = parseCssObject();
    var popoverdata = {
        "TemplateId": tempId ? tempId : '',
        "CSS": style,
        "CssObject": JSON.stringify(Css),
        "PopoverHTMLObject": JSON.stringify(popoverHTMLObject),
        "CompanyId": loggeduser.Company.CompanyId,

    };

    jQuery.ajax({
        url: api + "epc/savemaster",
        type: "POST",
        data: popoverdata,
        headers: {
            xaccesstoken: data.token,
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function (data) {
            if (data.updated) {

            }
            else {
                window.localStorage.setItem("MasterTemplateId", data.TemplateId);
            }
        }
    })
}

$('#remove-component').on('click', function (e) {
    e.preventDefault();
    if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
        $(".progress-wt").remove();
        popoverHTMLObject.isProgressbar = false;
    }
    if (globalSelector.getAttribute('id') == "step_number") {
        $("#step_number").next().remove();
        $("#step_number").remove();
        popoverHTMLObject.isStepGoto = false;
    }
    if (globalSelector.className.indexOf('popover-wt-title') !=-1) {
        $("#step_number").next().remove();
        $(".popover-wt-title").remove();
        popoverHTMLObject.isStepInfo = false;
        
    }
    if (globalSelector.className.indexOf('end') !=-1) {
       
        $(globalSelector).remove();
        popoverHTMLObject.BtnEnd = false;
        
    }


})

function changeMargins() {
    $('#mt-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';

        if (globalSelector.className.indexOf('popover-wt-title') != -1 ) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.marginT = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading') != -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.marginT = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.marginT = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.marginT = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.marginT = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.marginT = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.marginT = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.marginT = size;
        }
        $(globalSelector).css('margin-top', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })
    //margin-left
    $('#ml-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';

          if (globalSelector.className.indexOf('popover-wt-title') != -1 ) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.marginL = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading')!= -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.marginL = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.marginL = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.marginL = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.marginL = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.marginL = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.marginL = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.marginL = size;
        }
        $(globalSelector).css('margin-left', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })
    //margin-bottom
    $('#mb-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';

         if (globalSelector.className.indexOf('popover-wt-title') != -1) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.marginB = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading')!= -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.marginB = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.marginB = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.marginB = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.marginB = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.marginB = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.marginB = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.marginB = size;
        }
        $(globalSelector).css('margin-bottom', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })
    //margin-right
    $('#mr-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';

          if (globalSelector.className.indexOf('popover-wt-title') != -1 ) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.marginR = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading')!= -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.marginR = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.marginR = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.marginR = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.marginR = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.marginR = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.marginR = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.marginR = size;
        }
        $(globalSelector).css('margin-right', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })

}
function changePaddings() {
    $('#pt-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';

        if (globalSelector.className.indexOf('popover-wt') != -1) {
            Css.popover.hasProperties = true;
            Css.popover.paddingT = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading')!= -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.paddingT = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.paddingT = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.paddingT = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.paddingT = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.paddingT = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.paddingT = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.paddingT = size;
        }
        $(globalSelector).css('padding-top', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })
    //padding-left
    $('#pl-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';
        if (globalSelector.className.indexOf('popover-wt') != -1) {
            Css.popover.hasProperties = true;
            Css.popover.paddingL = size;
        }
        if (globalSelector.className.indexOf('popover-wt-title') != -1 ) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.paddingL = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading') != -1) {
            Css.heading.hasProperties = true;
            Css.heading.paddingL = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.paddingL = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.paddingL = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.paddingL = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.paddingL = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.paddingL = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.paddingL = size;
        }
        $(globalSelector).css('padding-left', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })
    //padding-bottom
    $('#pb-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';
        if (globalSelector.className.indexOf('popover-wt') != -1) {
            Css.popover.hasProperties = true;
            Css.popover.paddingB = size;
        }
        if (globalSelector.className.indexOf('popover-wt-title') != -1) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.paddingB = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading') != -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.paddingB = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.paddingB = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.paddingB = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.paddingB = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.paddingB = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.paddingB = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.paddingB = size;
        }
        $(globalSelector).css('padding-bottom', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })
    //padding-right
    $('#pr-wt').on('input keyup', function (e) {
        size = $(this).val() + 'px';
        if (globalSelector.className.indexOf('popover-wt') != -1) {
            Css.popover.hasProperties = true;
            Css.popover.paddingR = size;
        }
        if (globalSelector.className.indexOf('popover-wt-title') != -1) {
            Css.stepsTitle.hasProperties = true;
            Css.stepsTitle.paddingR = size;
        }
        if (globalSelector.className.indexOf('popover-wt-heading') != -1 ) {
            Css.heading.hasProperties = true;
            Css.heading.paddingR = size;
        }
        if (globalSelector.className.indexOf('popover-wt-content') != -1) {
            Css.content.hasProperties = true;
            Css.content.paddingR = size;
        }
        if (globalSelector.className.indexOf('close-wt') != -1) {
            Css.closeButton.hasProperties = true;
            Css.closeButton.paddingR = size;
        }
        if (globalSelector.className.indexOf('progress-wt-bar') != -1) {
            Css.progressbar.hasProperties = true;
            Css.progressbar.paddingR = size;
        }
        if (globalSelector.className.indexOf('next') != -1) {
            Css.nextButton.hasProperties = true;
            Css.nextButton.paddingR = size;
        }
        if (globalSelector.className.indexOf('end') != -1) {
            Css.endButton.hasProperties = true;
            Css.endButton.paddingR = size;

        }
        if (globalSelector.className.indexOf('back') != -1) {
            Css.backButton.hasProperties = true;
            Css.backButton.paddingR = size;
        }
        $(globalSelector).css('padding-right', size);
        if(globalSelector!=undefined) 
        stepbackarr.push(JSON.stringify(Css));
    })

}
//change button texts
function changeTextControls(){
     var inp = "<div id='button-wrapper'><label>Button Text</label> <input type='text' id='button-text' />"
    $('#controls').append(inp);
}
function changeText(){
    
        $("#button-text").on('input',function(e){
            if(globalSelector.className.indexOf('next')!=-1){
            $(".next").html($(this).val());
            popoverHTMLObject.BtnNext = globalSelector;
        }
        if(globalSelector.className.indexOf('back')!=-1){
            $(".back").html($(this).val());
            popoverHTMLObject.BtnPrev = globalSelector;
        }
          if(globalSelector.className.indexOf('end')!=-1){
            $(".end").html($(this).val());
            popoverHTMLObject.BtnEnd = globalSelector;
            }
        })
    
}

function showComponenCss(){
    if(globalSelector.className.indexOf("popover-wt")!=-1){
        //background color
       Css.popover.backgroundcolor?$("#chosen-value").val(Css.popover.backgroundcolor.replace("#","")):$("#chosen-value").val(rgb2hex($('.popover-wt').css('background-color')));
       Css.popover.borderRadius?$("#border-radius").val(Css.popover.borderRadius.replace("px","")):$("#border-radius").val($(".popover-wt").css("border-radius").replace("px",""));
       Css.popover.paddingT?$("#pt-wt").val(Css.popover.paddingT.replace("px","")):$("#pt-wt").val($(".popover-wt").css("padding-top").replace("px",""));
       Css.popover.paddingL?$("#pl-wt").val(Css.popover.paddingL.replace("px","")):$("#pl-wt").val($(".popover-wt").css("padding-left").replace("px",""));
       Css.popover.paddingB?$("#pb-wt").val(Css.popover.paddingB.replace("px","")):$("#pb-wt").val($(".popover-wt").css("padding-bottom").replace("px",""));
       Css.popover.paddingR?$("#pr-wt").val(Css.popover.paddingR.replace("px","")):$("#pr-wt").val($(".popover-wt").css("padding-right").replace("px",""));
       Css.popover.marginT?$("#mt-wt").val(Css.popover.marginT.replace("px","")):$("#mt-wt").val($(".popover-wt").css("margin-top").replace("px",""));
       Css.popover.marginL?$("#ml-wt").val(Css.popover.marginL.replace("px","")):$("#ml-wt").val($(".popover-wt").css("margin-left").replace("px",""));
       Css.popover.marginB?$("#mb-wt").val(Css.popover.marginB.replace("px","")):$("#mb-wt").val($(".popover-wt").css("margin-bottom").replace("px",""));
       Css.popover.marginR?$("#mr-wt").val(Css.popover.marginR.replace("px","")):$("#mr-wt").val($(".popover-wt").css("margin-right").replace("px",""));
    }
    if(globalSelector.className.indexOf("popover-wt-title")!=-1){
       Css.stepsTitle.color?$("#chosen-value-text").val(Css.stepsTitle.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.popover-wt-title').css('color')));
       Css.stepsTitle.fontSize?$("#font-size").val(Css.stepsTitle.fontSize.replace("#","")):$("#font-size").val($('.popover-wt-title').css('font-size').replace("px",""));
       Css.stepsTitle.font?$("#font-family").val(Css.stepsTitle.font):$("#font-family").val($('.popover-wt-title').css('font-family'));
       Css.stepsTitle.fontWeight?$("#font-weight").val(Css.stepsTitle.fontWeight):$("#font-weight").val($('.popover-wt-title').css('font-weight'));
       Css.stepsTitle.marginT?$("#mt-wt").val(Css.stepsTitle.marginT.replace("px","")):$("#mt-wt").val($(".popover-wt-title").css("margin-top").replace("px",""));
       Css.stepsTitle.marginL?$("#ml-wt").val(Css.stepsTitle.marginL.replace("px","")):$("#ml-wt").val($(".popover-wt-title").css("margin-left").replace("px",""));
       Css.stepsTitle.marginB?$("#mb-wt").val(Css.stepsTitle.marginB.replace("px","")):$("#mb-wt").val($(".popover-wt-title").css("margin-bottom").replace("px",""));
       Css.stepsTitle.marginR?$("#mr-wt").val(Css.stepsTitle.marginR.replace("px","")):$("#mr-wt").val($(".popover-wt-title").css("margin-right").replace("px",""));
    }
    if(globalSelector.className.indexOf("popover-wt-heading")!=-1 ){
       Css.heading.color?$("#chosen-value-text").val(Css.heading.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.popover-wt-title').css('color')));
       Css.heading.fontSize?$("#font-size").val(Css.heading.fontSize.replace("#","")):$("#font-size").val($('.popover-wt-title').css('font-size').replace("px",""));
       Css.heading.font?$("#font-family").val(Css.heading.font):$("#font-family").val($('.popover-wt-title').css('font-family'));
       Css.heading.fontWeight?$("#font-weight").val(Css.heading.fontWeight):$("#font-weight").val($('.popover-wt-title').css('font-weight'));
       Css.heading.marginT?$("#mt-wt").val(Css.heading.marginT.replace("px","")):$("#mt-wt").val($(".popover-wt-title").css("margin-top").replace("px",""));
       Css.heading.marginL?$("#ml-wt").val(Css.heading.marginL.replace("px","")):$("#ml-wt").val($(".popover-wt-title").css("margin-left").replace("px",""));
       Css.heading.marginB?$("#mb-wt").val(Css.heading.marginB.replace("px","")):$("#mb-wt").val($(".popover-wt-title").css("margin-bottom").replace("px",""));
       Css.heading.marginR?$("#mr-wt").val(Css.heading.marginR.replace("px","")):$("#mr-wt").val($(".popover-wt-title").css("margin-right").replace("px",""));
    }
     if(globalSelector.className.indexOf("next")!=-1){
       Css.nextButton.color?$("#chosen-value").val(Css.nextButton.color.replace("#","")):$("#chosen-value").val(rgb2hex($('.next').css('background-color')));  
       Css.nextButton.color?$("#chosen-value-text").val(Css.nextButton.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.next').css('color')));
       Css.nextButton.fontSize?$("#font-size").val(Css.nextButton.fontSize.replace("#","")):$("#font-size").val($('.next').css('font-size').replace("px",""));
       Css.nextButton.borderRadius?$("#border-radius").val(Css.nextButton.borderRadius.replace("px","")):$("#border-radius").val($('.next').css('border-radius').replace("px",""));
       Css.nextButton.paddingT?$("#pt-wt").val(Css.nextButton.paddingT.replace("px","")):$("#pt-wt").val($(".next").css("padding-top").replace("px",""));
       Css.nextButton.paddingL?$("#pl-wt").val(Css.nextButton.paddingL.replace("px","")):$("#pl-wt").val($(".next").css("padding-left").replace("px",""));
       Css.nextButton.paddingB?$("#pb-wt").val(Css.nextButton.paddingB.replace("px","")):$("#pb-wt").val($(".next").css("padding-bottom").replace("px",""));
       Css.nextButton.paddingR?$("#pr-wt").val(Css.nextButton.paddingR.replace("px","")):$("#pr-wt").val($(".next").css("padding-right").replace("px","")); 
       Css.nextButton.marginT?$("#mt-wt").val(Css.nextButton.marginT.replace("px","")):$("#mt-wt").val($(".next").css("margin-top").replace("px",""));
       Css.nextButton.marginL?$("#ml-wt").val(Css.nextButton.marginL.replace("px","")):$("#ml-wt").val($(".next").css("margin-left").replace("px",""));
       Css.nextButton.marginB?$("#mb-wt").val(Css.nextButton.marginB.replace("px","")):$("#mb-wt").val($(".next").css("margin-bottom").replace("px",""));
       Css.nextButton.marginR?$("#mr-wt").val(Css.nextButton.marginR.replace("px","")):$("#mr-wt").val($(".next").css("margin-right").replace("px",""));
    }
    if(globalSelector.className.indexOf("back")!=-1){
       Css.backButton.color?$("#chosen-value").val(Css.backButton.color.replace("#","")):$("#chosen-value").val(rgb2hex($('.back').css('background-color')));  
       Css.backButton.color?$("#chosen-value-text").val(Css.backButton.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.back').css('color')));
       Css.backButton.fontSize?$("#font-size").val(Css.backButton.fontSize.replace("#","")):$("#font-size").val($('.back').css('font-size').replace("px",""));
       Css.backButton.borderRadius?$("#border-radius").val(Css.backButton.borderRadius.replace("px","")):$("#border-radius").val($('.back').css('border-radius').replace("px",""));
       Css.backButton.paddingT?$("#pt-wt").val(Css.backButton.paddingT.replace("px","")):$("#pt-wt").val($(".back").css("padding-top").replace("px",""));
       Css.backButton.paddingL?$("#pl-wt").val(Css.backButton.paddingL.replace("px","")):$("#pl-wt").val($(".back").css("padding-left").replace("px",""));
       Css.backButton.paddingB?$("#pb-wt").val(Css.backButton.paddingB.replace("px","")):$("#pb-wt").val($(".back").css("padding-bottom").replace("px",""));
       Css.backButton.paddingR?$("#pr-wt").val(Css.backButton.paddingR.replace("px","")):$("#pr-wt").val($(".back").css("padding-right").replace("px","")); 
       Css.backButton.marginT?$("#mt-wt").val(Css.backButton.marginT.replace("px","")):$("#mt-wt").val($(".back").css("margin-top").replace("px",""));
       Css.backButton.marginL?$("#ml-wt").val(Css.backButton.marginL.replace("px","")):$("#ml-wt").val($(".back").css("margin-left").replace("px",""));
       Css.backButton.marginB?$("#mb-wt").val(Css.backButton.marginB.replace("px","")):$("#mb-wt").val($(".back").css("margin-bottom").replace("px",""));
       Css.backButton.marginR?$("#mr-wt").val(Css.backButton.marginR.replace("px","")):$("#mr-wt").val($(".back").css("margin-right").replace("px",""));
    }
    if(globalSelector.className.indexOf("end")!=-1){
       Css.endButton.color?$("#chosen-value").val(Css.endButton.color.replace("#","")):$("#chosen-value").val(rgb2hex($('.end').css('background-color')));  
       Css.endButton.color?$("#chosen-value-text").val(Css.endButton.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.end').css('color')));
       Css.endButton.fontSize?$("#font-size").val(Css.endButton.fontSize.replace("#","")):$("#font-size").val($('.end').css('font-size').replace("px",""));
       Css.endButton.borderRadius?$("#border-radius").val(Css.endButton.borderRadius.replace("px","")):$("#border-radius").val($('.end').css('border-radius').replace("px",""));
       Css.endButton.paddingT?$("#pt-wt").val(Css.endButton.paddingT.replace("px","")):$("#pt-wt").val($(".end").css("padding-top").replace("px",""));
       Css.endButton.paddingL?$("#pl-wt").val(Css.endButton.paddingL.replace("px","")):$("#pl-wt").val($(".end").css("padding-left").replace("px",""));
       Css.endButton.paddingB?$("#pb-wt").val(Css.endButton.paddingB.replace("px","")):$("#pb-wt").val($(".end").css("padding-bottom").replace("px",""));
       Css.endButton.paddingR?$("#pr-wt").val(Css.endButton.paddingR.replace("px","")):$("#pr-wt").val($(".end").css("padding-right").replace("px","")); 
       Css.endButton.marginT?$("#mt-wt").val(Css.endButton.marginT.replace("px","")):$("#mt-wt").val($(".end").css("margin-top").replace("px",""));
       Css.endButton.marginL?$("#ml-wt").val(Css.endButton.marginL.replace("px","")):$("#ml-wt").val($(".end").css("margin-left").replace("px",""));
       Css.endButton.marginB?$("#mb-wt").val(Css.endButton.marginB.replace("px","")):$("#mb-wt").val($(".end").css("margin-bottom").replace("px",""));
       Css.endButton.marginR?$("#mr-wt").val(Css.endButton.marginR.replace("px","")):$("#mr-wt").val($(".end").css("margin-right").replace("px",""));
    }
    if(globalSelector.className.indexOf("close-wt")!=-1){
       Css.closeButton.color?$("#chosen-value").val(Css.closeButton.color.replace("#","")):$("#chosen-value").val(rgb2hex($('.close-wt').css('background-color')));  
       Css.closeButton.color?$("#chosen-value-text").val(Css.closeButton.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.close-wt').css('color')));
       Css.closeButton.fontSize?$("#font-size").val(Css.closeButton.fontSize.replace("#","")):$("#font-size").val($('.close-wt').css('font-size').replace("px",""));
       Css.closeButton.borderRadius?$("#border-radius").val(Css.closeButton.borderRadius.replace("px","")):$("#border-radius").val($('.close-wt').css('border-radius').replace("px",""));
       Css.closeButton.paddingT?$("#pt-wt").val(Css.closeButton.paddingT.replace("px","")):$("#pt-wt").val($(".close-wt").css("padding-top").replace("px",""));
       Css.closeButton.paddingL?$("#pl-wt").val(Css.closeButton.paddingL.replace("px","")):$("#pl-wt").val($(".close-wt").css("padding-left").replace("px",""));
       Css.closeButton.paddingB?$("#pb-wt").val(Css.closeButton.paddingB.replace("px","")):$("#pb-wt").val($(".close-wt").css("padding-bottom").replace("px",""));
       Css.closeButton.paddingR?$("#pr-wt").val(Css.closeButton.paddingR.replace("px","")):$("#pr-wt").val($(".close-wt").css("padding-right").replace("px","")); 
       Css.closeButton.marginT?$("#mt-wt").val(Css.closeButton.marginT.replace("px","")):$("#mt-wt").val($(".close-wt").css("margin-top").replace("px",""));
       Css.closeButton.marginL?$("#ml-wt").val(Css.closeButton.marginL.replace("px","")):$("#ml-wt").val($(".close-wt").css("margin-left").replace("px",""));
       Css.closeButton.marginB?$("#mb-wt").val(Css.closeButton.marginB.replace("px","")):$("#mb-wt").val($(".close-wt").css("margin-bottom").replace("px",""));
       Css.closeButton.marginR?$("#mr-wt").val(Css.closeButton.marginR.replace("px","")):$("#mr-wt").val($(".close-wt").css("margin-right").replace("px",""));
    }
    if(globalSelector.className.indexOf("popover-wt-content")!=-1){
       //Css.s.color?$("#chosen-value").val(Css.endButton.color.replace("#","")):$("#chosen-value").val(rgb2hex($('.end').css('background-color')));  
       Css.content.color?$("#chosen-value-text").val(Css.content.color.replace("#","")):$("#chosen-value-text").val(rgb2hex($('.popover-wt-content').css('color')));
       Css.content.fontSize?$("#font-size").val(Css.content.fontSize.replace("#","")):$("#font-size").val($('.popover-wt-content').css('font-size').replace("px",""));
       Css.content.borderRadius?$("#border-radius").val(Css.content.borderRadius.replace("px","")):$("#border-radius").val($('.popover-wt-content').css('border-radius').replace("px",""));
       Css.content.paddingT?$("#pt-wt").val(Css.content.paddingT.replace("px","")):$("#pt-wt").val($(".popover-wt-content").css("padding-top").replace("px",""));
       Css.content.paddingL?$("#pl-wt").val(Css.content.paddingL.replace("px","")):$("#pl-wt").val($(".popover-wt-content").css("padding-left").replace("px",""));
       Css.content.paddingB?$("#pb-wt").val(Css.content.paddingB.replace("px","")):$("#pb-wt").val($(".popover-wt-content").css("padding-bottom").replace("px",""));
       Css.content.paddingR?$("#pr-wt").val(Css.content.paddingR.replace("px","")):$("#pr-wt").val($(".popover-wt-content").css("padding-right").replace("px","")); 
       Css.content.marginT?$("#mt-wt").val(Css.content.marginT.replace("px","")):$("#mt-wt").val($(".popover-wt-content").css("margin-top").replace("px",""));
       Css.content.marginL?$("#ml-wt").val(Css.content.marginL.replace("px","")):$("#ml-wt").val($(".popover-wt-content").css("margin-left").replace("px",""));
       Css.content.marginB?$("#mb-wt").val(Css.content.marginB.replace("px","")):$("#mb-wt").val($(".popover-wt-content").css("margin-bottom").replace("px",""));
       Css.content.marginR?$("#mr-wt").val(Css.content.marginR.replace("px","")):$("#mr-wt").val($(".popover-wt-content").css("margin-right").replace("px",""));
    }
}



//helper function
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return (hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3])).toUpperCase();
}
controlFlow();

function controlFlow(){
$(".control-flow-wt").on("change",function(){
    popoverHTMLObject.endOnClickingOutside = $(this).val();
})
}

function saveCheckpoint(name) {
    var loggeduser = JSON.parse(window.localStorage.getItem('authUser'));
    var tempId = window.localStorage.getItem("CheckpointId");
    var data = {};
    data.userId = loggeduser.Id;

    data.token = window.localStorage.getItem('token');
    var style = parseCssObject();
    var popoverdata = {
        "CheckpointName":name,
        "CheckpointId": tempId ? tempId : '',
        "CSS": style,
        "CssObject": JSON.stringify(Css),
        "PopoverHTMLObject": JSON.stringify(popoverHTMLObject),
        "CompanyId": loggeduser.Company.CompanyId,

    };

    jQuery.ajax({
        url: api + "epc/savecheckpoint",
        type: "POST",
        data: popoverdata,
        headers: {
            xaccesstoken: data.token,
        },
        contentType: "application/x-www-form-urlencoded; charset=utf-8",
        success: function (data) {
            if (data.updated) {

            }
            else {
                window.localStorage.setItem("CheckpointId", data.CheckpointId);
            }
        }
    })
}



